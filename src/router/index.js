import {
  createRouter,
  createWebHistory
} from 'vue-router'


const routes = [{
    path: '/',
    name: 'RegisterView',
    component: () => import('../views/RegisterView.vue')
  },
  {
    path: '/:username',
    name: 'ProfileView',
    component: () => import('../views/ProfileView.vue'),
  },
  {
    path: '/posts/:id',
    name: 'SingleView',
    component: () => import('../views/SingleView.vue'),
    props: true,
  },
  {
    path: '/post/create',
    name: 'StoryWritingView',
    component: () => import('../views/StoryWritingView.vue'),
    props: true,
  },
  {
    path: '/login',
    name: 'Register1View',
    component: () => import('../views/Register1View.vue')
  },
  {
    path: '/password',
    name: 'LoginView',
    component: () => import('../views/LoginView.vue')
  },
  {
    path: '/register',
    name: 'Register2View',
    component: () => import('../views/Register2View.vue')
  },
  {
    path: '/register_',
    name: 'Register2_View',
    component: () => import('../views/Register2_View.vue')
  },
  {
    path: '/register_done',
    name: 'RegisterDoneView',
    component: () => import('../views/RegisterDoneView.vue')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from) => {
  let isLogged = localStorage.getItem('loggedInUser') ? true : false;

  if ((to.path === '/login' || to.path === '/password' || to.path === '/register' || to.path === '/register_' || to.path === '/register_done') && isLogged) {
    return {
      name: 'RegisterView'
    }
  }
  if ((to.path === '/post/create') && !isLogged) {
    return {
      name: 'LoginView'
    }
  }
  if (from.path !== '/register' && to.path === '/register_' && !isLogged) {
    return {
      path: '/register'
    }
  }
})

export default router